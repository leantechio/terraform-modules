resource "aws_elastic_beanstalk_application" "application" {
  name        = var.EB_APP_NAME
  description = "Application for ${var.EB_APP_NAME}"
}
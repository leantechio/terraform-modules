output EB_DOMAIN {
    value = "${var.EB_ENV_NAME} -> ${aws_elastic_beanstalk_environment.env.endpoint_url}"
}
variable "SECURITY_GROUP" {}
variable "VPC_DEFAULT" {}
variable "AWS_ACM_ARN" {}
variable "SUBNETS_DEFAULT" {}

variable "EB_ENV_NAME" {}
variable "EB_SCALE_MIN" {}
variable "EB_SCALE_MAX" {}
variable "EB_APP_NAME" {}
variable "EB_APP_PORT" {}
variable "EB_STACK" {}
variable "EB_INSTANCE_TYPE" {}
variable "EB_TIMEOUT" {}
variable "EB_REQUEST_TIME" {}
variable "KEY_PAIR" {}

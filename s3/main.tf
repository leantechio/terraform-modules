resource "aws_s3_bucket" "b" {
  bucket = var.S3_BUCKET
  acl    = var.S3_PUBLIC
}

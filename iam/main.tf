resource "aws_iam_instance_profile" "beanstalk_role_service" {
  name = "beanstalk-ec2-profile-role"
  role = aws_iam_role.beanstalk_ec2_role.name
}
resource "aws_iam_role" "beanstalk_ec2_role" {
  name               = "beanstalk-ec2-role"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": [
                   "s3.amazonaws.com",
                   "ec2.amazonaws.com",
                   "cloudwatch.amazonaws.com"
                   ]
            },
            "Effect": "Allow",
            "Sid": "ec2Role"
        }
    ]
}
EOF
}

resource "aws_db_instance" "development" {
  allocated_storage    = var.DB_SIZE
  engine               = var.DB_ENGINE
  engine_version       = var.DB_ENGINE_VERSION
  instance_class       = var.DB_INSTANCE_TYPE
  name                 = var.DB_NAME
  username             = "admin"
  password             = var.DB_PASSWORD
  skip_final_snapshot  = true
  identifier           = var.APP_NAME
  publicly_accessible  = var.DB_PUBLIC
  vpc_security_group_ids = var.SECURITY_GROUP
}
output S3_WEB_DOMAIN {
    value = aws_s3_bucket.b.website_endpoint
}
output S3_DOMAIN {
    value = aws_s3_bucket.b.bucket_domain_name
}
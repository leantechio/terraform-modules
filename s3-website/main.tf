resource "aws_s3_bucket" "b" {
  bucket = var.S3_BUCKET
  acl    = "public-read"
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}
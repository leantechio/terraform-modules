# module "cdn" {
#   source = "cloudposse/cloudfront-cdn/aws"
#   namespace        = "${var.APP_NAME}"
#   stage            = "${var.APP_NAME}"
#   name             = "${var.APP_NAME}"
#   aliases          = ["${var.APP_NAME}.${var.DOMAIN}"]
#   acm_certificate_arn = "${var.AWS_ACM_ARN}"
#   comment = "${var.APP_NAME}"
#   viewer_protocol_policy = "redirect-to-https"

  
  
#   # Logs
#   cloudfront_access_log_create_bucket = false
#   cloudfront_access_log_bucket_name = "${var.APP_NAME}"

#   #Origin
#   custom_origins = [{
#     "domain_name" : var.S3_DOMAIN_NAME 
#     "origin_id" : var.APP_NAME
#     "s3_origin_config" : {
#       "origin_access_identity" : var.OAI
#     }
#   }]
# }
resource "aws_cloudfront_distribution" "distribution" {
  enabled = true
  comment = var.APP_NAME
  default_root_object = "index.html"
  
  origin {
    domain_name = var.S3_DOMAIN_NAME
    origin_id   = var.APP_NAME

    s3_origin_config {
      origin_access_identity = var.OAI
    }
  }
  logging_config {
    include_cookies = false
    bucket          = "${var.S3_DOMAIN_NAME}"
    prefix          = "cdn/"
  }
  default_cache_behavior {
    # ... other configuration ...
    target_origin_id = var.APP_NAME
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    # Cache
    max_ttl = 120
    min_ttl = 0
    default_ttl = 60
    viewer_protocol_policy = "redirect-to-https"
    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }
  viewer_certificate {
    acm_certificate_arn = var.AWS_ACM_ARN
    ssl_support_method = "sni-only"
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}
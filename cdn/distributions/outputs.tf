output CDN_DOMAIN {
    value = aws_cloudfront_distribution.distribution.domain_name
}
output CDN_ID {
    value = aws_cloudfront_distribution.distribution.id
}